import java.util.Iterator;

public class Main {

	public static void main(String[] args) {

		CustomerRecords records = new CustomerRecords();
		Integer i = 74;
		String s = i.toString().intern();
		records.addCustomer(new Customer("John"));
		records.addCustomer(new Customer("Simon"));
		
		
		// Ejemplo original, aca recorro el array y lo printeo, 
		//	pero podria borrar el mapa con un clear desde cualquier lado de la aplicacion.
		records.getCustomers().clear();
		
//		for(Customer next : records.getCustomers().values())
//		{
//			System.out.println(next);
//		}
		
		// Aca ya se puede iterar y usarlo en el for, porque records es un iterable.
		for(ReadonlyCustomer next : records) {
			System.out.println(next);
		}
		
		
//		Iterator<Customer> it = records.iterator();
//		
//		it.next();
//		it.remove();
//		
//		for(Customer next : records) {
//			System.out.println(next);
//		}
	}

}
