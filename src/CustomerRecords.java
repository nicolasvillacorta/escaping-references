import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CustomerRecords implements Iterable<Customer> {

		private Map<String, Customer> records;
		
		public CustomerRecords(){
			this.records = new HashMap<String, Customer>();
		}
		
		public void addCustomer(Customer c){
			this.records.put(c.getName(), c);
		}	

		public Map<String, Customer> getCustomers(){
			return Map.copyOf(this.records); // Java 10 o superior.
//			return Collections.unmodifiableMap(this.records); // Java 9 para abajo.
//			return new HashMap<>(this.records); // Con esta se aloca una nueva lista, pero sus posiciones referencia a los objetos original, asi que aun puedo mutarlos.
		}

		@Override
		public Iterator<Customer> iterator() {
			return records.values().iterator();
		}

		public ReadonlyCustomer find(String name) {
			return records.get(name);
		}
}